(function ($, doc, win) {
	'use strict'

	/* Admin page ready */
	$(doc).ready(function () {
		$('.color-field').wpColorPicker();

		var $colorSelectionField = $('#color-selection-field');

		/* Change the visibility of the color fields */
		$colorSelectionField.on('change', function() {
			$('fieldset[data-select]').hide();
			$('fieldset[data-select="' + $(this).val() + '"]').show();
		});

		/* Init the visibility of the color fields */
		$('fieldset[data-select]').hide();
		$('fieldset[data-select="' + $colorSelectionField.val() + '"]').show();
	});
})(jQuery, document, window);