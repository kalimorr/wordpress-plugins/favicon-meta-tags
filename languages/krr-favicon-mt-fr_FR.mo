��    %      D      l      l  D   m  !   �     �     �                     2     G     P  
   Y     d     �     �  (   �     �  0   �          %     ,     B     R     j  6   s  8   �     �  :   �     8     H  1   Q  ?   �  `   �  h   $  ;   �     �     �  �  �  `   �  (   �  "   	     <	     O	     ^	     s	     �	     �	     �	     �	  #   �	  !   �	     
  *   
     @
  :   Z
     �
     �
     �
     �
     �
     �
  ?   �
  B   ?     �  ?   �     �     �  <   �  L   /  p   |  |   �  B   j     �  &   �   A plugin that create your favicon meta tags for you for all devices. Android-Chrome - Background color Android-Chrome - Theme color Apple touch icon Chrome/Android Detected Files Favicon Meta Tags For each application Globally Kofinorr Let's go ! MS Applications - Theme color MS Applications - Title color MS tile Modern equivalent of original ICO format Original icon format Path to your favicon folder in your active theme Primary color Safari Safari pinned tab SVG Secondary color Selection of the colors Settings The files nomenclature is like this: favicon-32x32.png The main favicon is missing (format: PNG, sizes: 32px) ! The path does not exist ! The plugin Favicon MetaTags needs to be configured => %1$s Update settings Warnings We have found any favicon in the specified path ! We recommend you to add at least the following set of elements: You have at least one icon for Android but one or more colors are missing for this application ! You have at least one icon for MS applications but one or more colors are missing for this application ! You have the mask icon for Safari but no color associated ! https://github.com/kofinorr iOS - Safari pinned color Project-Id-Version: Favicon MetaTags
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-05-10 20:22+0000
PO-Revision-Date: 2021-04-06 20:32+0000
Last-Translator: 
Language-Team: Français
Language: fr_FR
Plural-Forms: nplurals=2; plural=n > 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.5.2; wp-5.7 Un plugin qui créé vos balises de favicon pour vous, avec une compatibilité sur tout support. Android-Chrome - Couleur d'arrière-plan Android-Chrome - Couleur du thème Icône Apple Touch Chrome/Android Fichiers détéctés Favicon Meta Tags Pour chaque application Global Kofinorr C'est parti ! Applications MS - Couleur du thème Application MS - Couleur du titre Tuile de Microscoft Équivalent moderne du format ICO original Format original de l'icon Chemin vers le dossier "favicon" depuis votre thème actif Couleur primaire Safari Onglet épinglé de Safari Couleur secondaire Sélection des couleurs Paramètres La nomenclature de fichier est la suivante :  favicon-32x32.png Le favicon principal est introuvable (format: PNG, taille: 32px) ! Le chemin n'existe pas ! Le plugin Favicon Meta Tags a besoin d'être configuré => %1$s Mettre à jour Erreurs Nous n'avons trouvé aucun favicon dans le chemin indiqué ! Nous vous recommandons d'ajouter au moins la liste des éléments suivants : Vous avez au moins un icône pour Android mais une ou plusieurs couleurs sont manquante pour cette application ! Vous avez au moins un icône pour les applications MS mais une ou plusieurs couleurs sont manquante pour cette application ! Vous avez le mask-icon pour Safari mais pas de couleur associée ! https://github.com/kofinorr iOS - Safari couleur d’épinglement  