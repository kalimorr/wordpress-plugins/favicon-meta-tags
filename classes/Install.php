<?php

namespace KrrFaviconMetaTags;

/**
 * Class Install
 * @package KrrFaviconMetaTags
 */
class Install
{

	/**
	 * On plugin activation
	 */
	public static function activation()
	{
		mkdir(WP_CONTENT_DIR . '/' . Plugin::getInstance()->getConfigFolder());
	}

	/**
	 * On plugin deactivation
	 */
	public static function deactivation()
	{
		self::removeDir(WP_CONTENT_DIR . '/' . Plugin::getInstance()->getConfigFolder());
		delete_option(Settings::getInstance()->getOptionName());
	}

	/**
	 * Remove a directory
	 * @param $path
	 */
	private static function removeDir($path)
	{
		if (is_dir($path) === true) {
			$files = array_diff(scandir($path), ['.', '..']);

			foreach ($files as $file) {
				self::removeDir(realpath($path) . '/' . $file);
			}

			rmdir($path);
		} else if (is_file($path) === true) {
			unlink($path);
		}
	}
}