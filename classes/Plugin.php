<?php

namespace KrrFaviconMetaTags;

/**
 * Class PLugin
 * @package KrrFaviconMetaTags
 */
class Plugin
{
	/**
	 * @var string Slug of the options Page
	 */
	private $pageSlug = 'favicon-metatags';

	/**
	 * @var string Path stored in options
	 */
	private $faviconMetaTagsPath = '';

	/**
	 * @var string Url stored in options
	 */
	private $faviconMetaTagsUrl = '';

	/**
	 * @var string Name of the config folder
	 */
	public $configFolder = 'krr-favicon-metatags';

	/**
	 * @var array Array of all sizes supported by this plugin
	 */
	public $sizes = [
		'favicon' => [
			'ico', 16, 32
		],
		'android-chrome' => [
			36, 48, 96, 144, 192, 256, 384, 512
		],
		'ios' => [
			'mask-icon', 57, 60, 72, 76, 114, 120, 152, 167, 180
		],
		'msapplication' => [
			126, 144, 270, '558x270', 558
		]
	];

	/**
	 * @var Plugin|null  Instance of the current class
	 */
	private static $instance = null;

	/**
	 * Plugin constructor.
	 */
	public function __construct()
	{
		Settings::getInstance();
		Generator::getInstance();
		add_action('plugins_loaded', [$this, 'load_i18n'], 10);
		add_action('init', [$this, 'setFaviconMetaTagPathUrl'], 1);
		add_action('admin_enqueue_scripts', [$this, 'enqueueScripts'], 11);
		add_action('admin_menu', [$this, 'addMenuPage']);
		add_action('admin_notices', [$this, 'addInitNotice']);
	}

	/**
	 * Get the instance of the current class
	 *
	 * @return Plugin|null
	 */
	public static function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new Plugin();
		}

		return self::$instance;
	}

	/**
	 * @return string
	 */
	public function getPageSlug(): string
	{
		return $this->pageSlug;
	}

	/**
	 * @return array
	 */
	public function getSizes(): array
	{
		return $this->sizes;
	}

	/**
	 * @return string
	 */
	public function getFaviconMetaTagPath(): string
	{
		return $this->faviconMetaTagsPath;
	}

	/**
	 * @return string
	 */
	public function getFaviconMetaTagUrl(): string
	{
		return $this->faviconMetaTagsUrl;
	}

	/**
	 * @return string
	 */
	public function getConfigFolder(): string
	{
		return $this->configFolder;
	}

	/**
	 * Get the Favicon Path from the options
	 */
	public function setFaviconMetaTagPathUrl()
	{
		$option = Settings::getInstance()->getValue('path');
		if (!is_null($option)) {
			$this->faviconMetaTagsPath = get_stylesheet_directory() . '/' . $option . '/favicon/';
			$this->faviconMetaTagsUrl = get_stylesheet_directory_uri() . '/' . $option . '/favicon/';
		}
	}

	/**
	 * Load plugin textdomain
	 */
	public function load_i18n()
	{
		load_plugin_textdomain(
			'krr-favicon-mt',
			false,
			dirname(plugin_basename(KRR_FAVICON_MT_FILE)) . '/languages'
		);
	}


	/**
	 *  Styles and scripts
	 */
	public function enqueueScripts()
	{
		if (!isset($_GET) || !isset($_GET['page']) || $_GET['page'] !== $this->getPageSlug()) {
			return;
		}

		// Add the color picker css file
		wp_enqueue_style( 'wp-color-picker' );

		wp_enqueue_style(
			'krr-favicon-mt-style',
			plugin_dir_url(KRR_FAVICON_MT_FILE) . 'assets/style.min.css',
			[],
			false
		);

		wp_enqueue_script('krr-favicon-mt-script',
			plugin_dir_url(KRR_FAVICON_MT_FILE) . 'assets/script.min.js',
			['wp-color-picker'],
			false,
			true
		);
	}

	/**
	 * Method to create the menu access to the options page
	 */
	public function addMenuPage()
	{
		add_submenu_page(
			'themes.php',
			__('Favicon Meta Tags', 'krr-favicon-mt'),
			__('Favicon Meta Tags', 'krr-favicon-mt'),
			'manage_options',
			$this->getPageSlug(),
			[$this, 'optionsPage']
		);
	}

	/**
	 * View of the options Page
	 */
	public function optionsPage()
	{
		Settings::getInstance()->saveOptions();
		$this->setFaviconMetaTagPathUrl();
		include_once plugin_dir_path(KRR_FAVICON_MT_FILE) . '/views/optionPage.php';
	}

	/**
	 * Check if a favicon size exists
	 * @param $device
	 * @param $size
	 * @return bool
	 */
	public function sizeExist($device, $size)
	{
		return file_exists($this->getFaviconFilePath($device, $size));
	}

	/**
	 * Check if a device has at least one icon file
	 * @param $device
	 * @return bool
	 */
	public function deviceHasSizes($device)
	{
		foreach ($this->getSizes()[$device] as $size) {
			if ($this->sizeExist($device, $size)) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Get the path to a favicon file
	 * @param $device
	 * @param $size
	 * @param bool $url
	 * @return string
	 */
	public function getFaviconFilePath($device, $size, $url = false)
	{
		$basePath = ($url) ? $this->getFaviconMetaTagUrl() : $this->getFaviconMetaTagPath();

		if (is_string($size)) {
			if ($size === 'ico') {
				return $basePath . $device . '.ico';
			} elseif ($size === 'mask-icon') {
				return $basePath . $size . '.svg';
			} else {
				return $basePath . $device . '-' . $size . '.png';
			}
		} else {
			return $basePath . $device . '-' . $size . 'x' . $size . '.png';
		}
	}

	/**
	 * Add a notice if the plugin has not been configured yet.
	 */
	public function addInitNotice ()
	{
		/* Check if the option exist */
		if (get_option(Settings::getInstance()->getOptionName())) {
			return;
		}

		/* Render the message */
		echo '<div class="notice notice-error"><p><strong>' . sprintf(
			__('The plugin Favicon MetaTags needs to be configured => %1$s', 'krr-favicon-mt'),
				sprintf(
					'<a href="%1$s">' . __('Let\'s go !', 'krr-favicon-mt') . '</a>',
					admin_url('themes.php?page=' . $this->getPageSlug())
				)
		) . '</strong></p></div>';
	}
}