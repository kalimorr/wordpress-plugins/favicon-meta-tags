<?php

namespace KrrFaviconMetaTags;

/**
 * Class Generator
 *
 * @package KrrFaviconMetaTags
 */
class Generator
{
	/**
	 * @var Generator|null  Instance of the current class
	 */
	private static $instance = null;

	/**
	 * Plugin constructor.
	 */
	public function __construct()
	{
		add_action('wp_head', [$this, 'addFaviconMetaTagTags']);
		add_action('admin_head', [$this, 'addFaviconMetaTagTags']);
	}

	/**
	 * Get the instance of the current class
	 *
	 * @return Generator|null
	 */
	public static function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new Generator();
		}

		return self::$instance;
	}

	/**
	 * Add favicon meta tags in front and admin
	 */
	public function addFaviconMetaTagTags()
	{
		$tags   = '';
		$plugin = Plugin::getInstance();

		/* By default we don't need the config files */
		$needBrowserConfig = false;
		$needManifest      = false;

		/* Get global color settings */
		$typeColors     = Settings::getInstance()->getValue('color-selection');
		$primaryColor   = Settings::getInstance()->getValue('primary-color');
		$secondaryColor = Settings::getInstance()->getValue('secondary-color');

		foreach ($plugin->getSizes() as $device => $deviceSizes) {
			foreach ($deviceSizes as $size) {
				/* Skip all undefined sizes files */
				if (!$plugin->sizeExist($device, $size)) {
					continue;
				}

				/* Reset the atts variables */
				$sizeAttr = '';
				$relAttr  = '';

				/* Mask icon for Safari */
				if ($device === 'ios' && $size === 'mask-icon') {
					/* Get color settings */
					if ($typeColors === 'global') {
						$iosColor = $primaryColor;
					} else {
						$iosColor = Settings::getInstance()->getValue('ios-color');
					}

					$tags     .= '<link rel="mask-icon" href="' . $plugin->getFaviconFilePath($device, $size, true) . '" color="' . $iosColor . '">';
				} /* Icon for MS apps */
				elseif ($device === 'msapplication') {
					/* Get color settings */
					if ($typeColors === 'global') {
						$msTitleColor = $secondaryColor;
						$msThemeColor = $primaryColor;
					} else {
						$msTitleColor = Settings::getInstance()->getValue('ms-title-color');
						$msThemeColor = Settings::getInstance()->getValue('ms-theme-color');
					}

					/* Specific render for the 144 size. For the others, we pass by the XML config */
					if ($size === 144) {
						$tags .= '<meta name="msapplication-TileColor" content="' . $msTitleColor . '">';
						$tags .= '<meta name="msapplication-TileImage" content="' . $plugin->getFaviconFilePath($device, $size, true) . '">';
						$tags .= '<meta name="theme-color" content="' . $msThemeColor . '">';
					} else {
						/* Indicate that we have to create the browserconfig.xml file for at least one file */
						$needBrowserConfig = true;
					}
				} /* Default favicon format */
				else {

					/* Indicate that we have to create the manifest.json file for at least one file */
					if ($device === 'android-chrome') {
						$needManifest = true;
					}

					/* Create the "sizes" attribute */
					if (!in_array($size, ['ico', 'mask-icon'])) {
						if (is_string($size)) {
							$sizeAttr = 'sizes="' . $size . '"';
						} else {
							$sizeAttr = 'sizes="' . $size . 'x' . $size . '"';
						}
					}

					/* Create the "rel" attribute */
					if ($device === 'favicon' || $device === 'android-chrome') {
						$relAttr = 'rel="icon"';
					} elseif ($device === 'ios') {
						$relAttr = 'rel="apple-touch-icon"';
					}

					/* Construction of the tag */
					$tags .= '<link ' . $relAttr . ' ' . $sizeAttr . ' href="' . $plugin->getFaviconFilePath($device, $size, true) . '">';
				}
			}

			if ($device === 'msapplication') {
				/* Browser config file */
				if ($needBrowserConfig) {
					$tags .= '<meta name="msapplication-config" content="' . content_url($plugin->getConfigFolder()) . '/browserconfig.xml" />';
				}
			} elseif ($device === 'android-chrome') {
				/* Manifest.json file */
				if ($needManifest) {
					$tags .= '<link rel="manifest" href="' . content_url($plugin->getConfigFolder()) . '/manifest.json" />';
				}
			}
		}

		/* Render all the tags */
		echo $tags;
	}

	/**
	 * XML for browserconfig file
	 */
	public function xmlConfig()
	{
		$plugin = Plugin::getInstance();
		$device = 'msapplication';

		/* Get global color settings */
		$typeColors     = Settings::getInstance()->getValue('color-selection');
		$secondaryColor = Settings::getInstance()->getValue('secondary-color');

		/* Create the main nodes */
		$xml    = new \DOMDocument('1.0', 'utf-8');
		$config = $xml->createElement('browserconfig');
		$app    = $xml->createElement('msapplication');
		$title  = $xml->createElement('tile');


		/* Sizes */
		foreach ($plugin->getSizes()[$device] as $size) {
			/* Skip all undefined sizes files */
			if (!$plugin->sizeExist($device, $size) || $size === 144) {
				continue;
			}

			$nodeName = null;

			/* Get the node name by the size */
			switch (strval($size)) {
				case '126':
					$nodeName = 'square70x70logo';
					break;
				case '270':
					$nodeName = 'square150x150logo';
					break;
				case '558x270':
					$nodeName = 'wide310x150logo';
					break;
				case '558':
					$nodeName = 'square310x310logo';
					break;
			}

			if ($nodeName === null) {
				continue;
			}

			/* Create the size node */
			$sizeNode = $xml->createElement($nodeName);
			$sizeNode->setAttribute('src', $plugin->getFaviconFilePath($device, $size, true));
			$title->appendChild($sizeNode);
		}


		/* Create the TitleColor node */
		$titleColor            = $xml->createElement('TitleColor');

		/* Color by type of selection */
		if ($typeColors === 'global') {
			$titleColor->nodeValue = $secondaryColor;
		} else {
			$titleColor->nodeValue = Settings::getInstance()->getValue('ms-title-color');
		}

		/* Add the different node to the document */
		$title->appendChild($titleColor);
		$app->appendChild($title);
		$config->appendChild($app);
		$xml->appendChild($config);

		/* Save the XML file */
		$xml->save(WP_CONTENT_DIR . '/' . Plugin::getInstance()->getConfigFolder() . '/browserconfig.xml');
	}

	/**
	 * JSON for the manifest file
	 */
	public function manifestConfig()
	{
		$plugin   = Plugin::getInstance();
		$settings = Settings::getInstance();
		$device   = 'android-chrome';

		/* Get global color settings */
		$typeColors     = Settings::getInstance()->getValue('color-selection');
		$primaryColor   = Settings::getInstance()->getValue('primary-color');
		$secondaryColor = Settings::getInstance()->getValue('secondary-color');

		/* Set the simple entries of the JSON file */
		$manifest = [
			'name'             => get_option('blogname'),
			'short_name'       => get_option('blogname'),
			'icons'            => [],
			'theme_color'      => null,
			'background_color' => null,
			'display'          => 'standalone'
		];

		/* Color by type of selection */
		if ($typeColors === 'global') {
			$manifest['theme_color'] = $secondaryColor;
			$manifest['background_color'] = $primaryColor;
		} else {
			$manifest['theme_color'] = $settings->getValue('android-theme-color');
			$manifest['background_color'] = $settings->getValue('android-background-color');
		}

		/* Sizes */
		foreach ($plugin->getSizes()[$device] as $size) {
			/* Skip all undefined sizes files */
			if (!$plugin->sizeExist($device, $size)) {
				continue;
			}

			/* Create an entry for each android icons */
			$manifest['icons'][] = [
				'src'   => $plugin->getFaviconFilePath($device, $size, true),
				'sizes' => $size . 'x' . $size,
				'type'  => 'image/png'
			];
		}

		/* Save the JSON file */
		file_put_contents(
			WP_CONTENT_DIR . '/' . Plugin::getInstance()->getConfigFolder() . '/manifest.json',
			json_encode($manifest)
		);
	}
}