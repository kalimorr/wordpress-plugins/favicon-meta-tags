<?php

namespace KrrFaviconMetaTags;

/**
 * Class Warnings
 *
 * @package KrrFaviconMetaTags
 */
class Warnings
{
	/**
	 * @var array Array of all errors
	 */
	private $errors = [];

	/**
	 * @var Warnings|null  Instance of the current class
	 */
	public static $instance = null;

	/**
	 * Get the instance of the current class
	 *
	 * @return Warnings|null
	 */
	public static function getInstance()
	{
		if (self::$instance === null) {
			self::$instance = new Warnings();
		}

		return self::$instance;
	}

	/**
	 * Show the error block
	 */
	public function show()
	{
		$this->setErrors();

		if (empty($this->getErrors())) {
			return;
		}

		include plugin_dir_path(KRR_FAVICON_MT_FILE) . '/views/warnings.php';
	}

	/**
	 * @return array
	 */
	public function getErrors(): array
	{
		return $this->errors;
	}

	/**
	 * Set the errors list
	 */
	public function setErrors()
	{
		$plugin   = Plugin::getInstance();
		$settings = Settings::getInstance();

		$this->errors = [];

		/* If no options, the plugin has not been configured yet */
		if (!get_option($settings->getOptionName())) {
			return;
		}

		$colorSelection = $settings->getValue('color-selection');
		$primaryColor   = $settings->getValue('primary-color');
		$secondaryColor = $settings->getValue('secondary-color');

		if (!file_exists($plugin->getFaviconMetaTagPath())) {
			$this->errors[] = __('The path does not exist !', 'krr-favicon-mt');
		}

		/* No favicon in path */
		if ($settings->checkEmptyFolder($plugin->getFaviconMetaTagPath())) {
			$this->errors[] = __('We have found any favicon in the specified path !', 'krr-favicon-mt');
		}

		/* Missing main favicon */
		if (!file_exists($plugin->getFaviconMetaTagPath() . 'favicon-32x32.png')) {
			$this->errors[] = __('The main favicon is missing (format: PNG, sizes: 32px) !', 'krr-favicon-mt');
		}

		/* Missing MS colors */
		if ($plugin->deviceHasSizes('msapplication')
			&& ($colorSelection === 'global' && (!$primaryColor || !$secondaryColor))
			|| ($colorSelection === 'specific' && (!$settings->getValue('ms-title-color') || !$settings->getValue('ms-theme-color')))
		) {
			$this->errors[] = __('You have at least one icon for MS applications but one or more colors are missing for this application !', 'krr-favicon-mt');
		}

		/* Missing Androïd-Chrome colors */
		if ($plugin->deviceHasSizes('android-chrome')
			&& ($colorSelection === 'global' && (!$primaryColor || !$secondaryColor))
			|| ($colorSelection === 'specific' && (!$settings->getValue('android-theme-color') || !$settings->getValue('android-background-color')))
		) {
			$this->errors[] = __('You have at least one icon for Android but one or more colors are missing for this application !', 'krr-favicon-mt');
		}

		/* Missing iOS colors */
		if (file_exists($plugin->getFaviconMetaTagPath() . 'mask-icon.svg') && !$settings->getValue('ios-color')) {
			$this->errors[] = __('You have the mask icon for Safari but no color associated !', 'krr-favicon-mt');
		}
	}
}